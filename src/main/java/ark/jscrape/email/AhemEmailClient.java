package ark.jscrape.email;

import ark.jscrape.net.Http;
import ark.jscrape.net.models.RequestProperty;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
public class AhemEmailClient {
    private static final String DOMAIN = "ahem.email";
    private static final String API_BASE = "https://www." + DOMAIN + "/api/";
    private static final String API_AUTH = "auth/token";
    private static final String API_MAILBOX = "mailbox/NAME/email/";
    private String token;
    private RequestProperty[] requestProperties;
    /**
     * @see "https://www.ahem.email/help/api"
     */
    public AhemEmailClient() throws IOException {
        this.token = this.requestToken();
        System.out.println("AhemEmailClient: Authorized!");
        this.requestProperties = new RequestProperty[] {
                new RequestProperty("Authorization", "Bearer " + this.token)
        };
    }

    public AhemEmail[] getMailbox(String mailboxId) throws IOException {
        JSONArray res = this.getJSONArray(this.getApiMailbox(mailboxId));
        AhemEmail[] emails = new AhemEmail[res.length()];
        for (int i = 0; i < emails.length; i++) {
            emails[i] = new AhemEmail(res.getJSONObject(i));
        }
        return emails;
    }

    public AhemEmail getEmail(String mailboxId, String emailId) throws IOException {
        return new AhemEmail(this.getJSONObject(this.getApiMailbox(mailboxId) + emailId));
    }

    public String mailboxIdToEmail(String mailboxId) {
        return mailboxId + "@" + DOMAIN;
    }

    private String requestToken() throws IOException {
        JSONObject object = new JSONObject(Http.post(API_BASE + API_AUTH, new RequestProperty[0], "{}"));
        if (!object.optBoolean("success", false)) throw new IOException("Unable to retrieve token from ahem server!");
        return object.optString("token", "BAD TOKEN");
    }

    private JSONObject getJSONObject(String uri) throws IOException {
        try {
            JSONObject object = new JSONObject(Http.get(uri, this.requestProperties));
            if (object.isEmpty()) throw new IOException("Empty response from the ahem server!");
            return object;
        } catch (FileNotFoundException e) {
            throw new IOException("Not found or empty: " + uri);
        }
    }

    private JSONArray getJSONArray(String uri) throws IOException {
        try {
            return new JSONArray(Http.get(uri, this.requestProperties));
        } catch (FileNotFoundException e) {
            throw new IOException("Not found or empty: " + uri);
        }
    }

    private JSONObject postJSONObject(String uri, JSONObject payload) throws IOException {
        try {
            JSONObject object = new JSONObject(Http.post(uri, this.requestProperties, payload.toString()));
            if (object.isEmpty()) throw new IOException("Empty response from the ahem server!");
            return object;
        } catch (FileNotFoundException e) {
            throw new IOException("Not found or empty: " + uri);
        }
    }
    private JSONArray postJSONArray(String uri, JSONArray payload) throws IOException {
        try {
            return new JSONArray(Http.post(uri, this.requestProperties, payload.toString()));
        } catch (FileNotFoundException e) {
            throw new IOException("Not found or empty: " + uri);
        }
    }

    private String getApiMailbox(String mailboxId) {
        return API_BASE + API_MAILBOX.replace("NAME", mailboxId);
    }
}
