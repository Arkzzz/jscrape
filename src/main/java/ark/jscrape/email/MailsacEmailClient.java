package ark.jscrape.email;

import ark.jscrape.net.Http;
import org.json.JSONArray;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

public class MailsacEmailClient {
    private static final String DOMAIN = "mailsac.com";
    private static final String API_BASE = "https://" + DOMAIN + "/api/";
    private static final String MAILBOX_API = "addresses/EMAIL/messages";

    public MailsacEmailClient() { }

    public MailsacEmail[] getEmails(String email) throws IOException {
        String uri = this.getMailboxApi(email);
        try {
            JSONArray emailsJson = new JSONArray(Http.get(uri));
            MailsacEmail[] emails = new MailsacEmail[emailsJson.length()];
            for (int i = 0; i < emails.length; i++) {
                emails[i] = new MailsacEmail(emailsJson.getJSONObject(i));
            }
            return emails;
        } catch (FileNotFoundException e) {
            throw new IOException("Not found or empty: " + uri);
        }
    }

    public String mailboxNameToEmail(String mailboxId) {
        return mailboxId + "@" + DOMAIN;
    }

    private String getMailboxApi(String email) {
        return API_BASE + MAILBOX_API.replace("EMAIL", email);
    }
}
