package ark.jscrape.email;

import org.json.JSONArray;
import org.json.JSONObject;

public class MailsacEmail {
    private String sender;
    private String subject;
    private String[] links;
    public MailsacEmail(JSONObject obj) {
        this.sender = obj.has("from") ? obj.getJSONArray("from").getJSONObject(0).getString("address") : "unknown";
        this.subject = obj.optString("subject", "unknown");
        if (obj.has("links")) {
            JSONArray linksJson = obj.getJSONArray("links");
                    this.links = new String[linksJson.length()];
                    for (int i = 0; i < this.links.length; i++) {
                        this.links[i] = linksJson.getString(i);
                    }
        } else {
            this.links = new String[0];
        }
    }

    public String getSender() {
        return sender;
    }

    public String getSubject() {
        return subject;
    }

    public String[] getLinks() {
        return links;
    }
}
