package ark.jscrape.email;

import org.json.JSONObject;

public class AhemEmail {
    private String emailId;
    private String text;
    private String sender;
    public AhemEmail(JSONObject obj) {
        this.emailId = obj.optString("emailId", "unknown");
        if (obj.has("_id")) this.emailId = obj.optString("_id", "unknown");
        this.text = obj.optString("text", "none");
        if (obj.has("sender")) {
            this.sender = obj.getJSONObject("sender").optString("address");
        }
    }

    public String getEmailId() {
        return emailId;
    }

    public String getText() {
        return text;
    }

    public String getSender() {
        return sender;
    }
}
