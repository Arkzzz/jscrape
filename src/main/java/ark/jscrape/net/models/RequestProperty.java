package ark.jscrape.net.models;

public class RequestProperty {
    private String key;
    private String value;
    public RequestProperty(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }
}
