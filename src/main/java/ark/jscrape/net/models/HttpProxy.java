package ark.jscrape.net.models;

import java.net.InetSocketAddress;
import java.net.Proxy;

public class HttpProxy {
    private String host;
    private int port;
    private long cooldown;

    public HttpProxy(String hostport) throws Exception {
        String[] split = hostport.split(":");
        this.host = split[0];
        this.port = Integer.parseInt(split[1]);
        this.cooldown = 0;
    }

    public HttpProxy(String host, int port) {
        this.host = host;
        this.port = port;
        this.cooldown = 0;
    }

    public Proxy getProxy() {
        return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.host, this.port));
    }

    public void setCooldown(long delayMs) {
        this.cooldown = System.currentTimeMillis() + delayMs;
    }

    public boolean available() {
        return System.currentTimeMillis() > this.cooldown;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
