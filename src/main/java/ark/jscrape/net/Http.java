package ark.jscrape.net;

import ark.jscrape.net.models.HttpProxy;
import ark.jscrape.net.models.RequestProperty;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

public class Http {
    public static final String USER_AGENT = "Google Chrome Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36.";
    public static final String CONTENT_TYPE = "application/json, */*";

    public static String get(String uri, int timeout, RequestProperty[] properties) throws IOException {
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        return get(conn, timeout, properties);
    }

    public static String get(String uri, RequestProperty[] properties) throws IOException {
        return get(uri, 5000, properties);
    }

    public static String get(String uri) throws IOException {
        return get(uri, 5000, new RequestProperty[0]);
    }

    public static String proxiedGet(String uri, int timeout, RequestProperty[] properties, HttpProxy proxy) throws IOException {
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy.getProxy());

        return get(conn, timeout, properties);
    }

    public static String proxiedGet(String uri, int timeout, HttpProxy proxy) throws IOException {
        return proxiedGet(uri, timeout, new RequestProperty[0], proxy);
    }

    public static String proxiedGet(String uri, HttpProxy proxy) throws IOException {
        return proxiedGet(uri, 5000, proxy);
    }

    @SuppressWarnings("Duplicates")
    private static String get(HttpURLConnection conn, int timeout, RequestProperty[] properties) throws IOException {
        StringBuilder result = new StringBuilder();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept", CONTENT_TYPE);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        for (RequestProperty p : properties) {
            conn.setRequestProperty(p.getKey(), p.getValue());
        }
        conn.setConnectTimeout(timeout);
        conn.setReadTimeout(timeout);
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        conn.disconnect();
        return result.toString();
    }

    public static String post(String uri, int timeout, RequestProperty[] properties, String payload) throws IOException {
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        return post(conn, timeout, properties, payload);
    }

    public static String post(String uri, RequestProperty[] properties, String payload) throws IOException {
        return post(uri, 5000, properties, payload);
    }

    @SuppressWarnings("Duplicates")
    private static String post(HttpURLConnection conn, int timeout, RequestProperty[] properties, String payload) throws IOException {
        StringBuilder result = new StringBuilder();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept", CONTENT_TYPE);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        for (RequestProperty p : properties) {
            conn.setRequestProperty(p.getKey(), p.getValue());
        }
        conn.setConnectTimeout(timeout);
        conn.setDoOutput(true);
        DataOutputStream w = new DataOutputStream(conn.getOutputStream());
        w.writeBytes(payload);
        w.flush();
        w.close();

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        conn.disconnect();
        return result.toString();
    }
}

