package ark.jscrape.net;

import ark.jscrape.concurency.IPushable;
import ark.jscrape.net.models.HttpProxy;

import java.util.ArrayList;
import java.util.Collection;

public class HttpProxyRotator implements IPushable<HttpProxy> {
    private ArrayList<HttpProxy> proxies;

    public HttpProxyRotator() {
        this.proxies = new ArrayList<>();
    }

    public void push(HttpProxy p) {
        this.proxies.add(p);
    }

    public void addAll(Collection<HttpProxy> proxies) {
        this.proxies.addAll(proxies);
    }

    public HttpProxy getProxy(long cooldown) {
        /*
        for (HttpProxy p : this.proxies) {
            if (p.available()) {
                p.setCooldown(cooldown);
                return p;
            }
        }
        */
        while (true) {
            HttpProxy p = this.proxies.get((int)((this.proxies.size() - 1) * Math.random()));
            if (p.available()) {
                return p;
            }
        }
    }
}
