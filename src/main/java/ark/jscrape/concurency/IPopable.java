package ark.jscrape.concurency;

public interface IPopable<T> {
    T pop();
}
