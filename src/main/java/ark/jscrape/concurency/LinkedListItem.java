package ark.jscrape.concurency;

public class LinkedListItem<T> {
    private T item;
    private LinkedListItem<T> next;

    public LinkedListItem(T item, LinkedListItem<T> next) {
        this.item = item;
        this.next = next;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public void setNext(LinkedListItem<T> next) {
        this.next = next;
    }

    public LinkedListItem<T> getNext() {
        return next;
    }
}
