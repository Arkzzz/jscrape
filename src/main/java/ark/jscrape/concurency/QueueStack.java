package ark.jscrape.concurency;

public class QueueStack<T> implements IPushable<T>, IPopable {
    private LinkedListItem<T> next;

    public QueueStack() { }

    /**
     * Push to the stack
     * @param item The item to push to the stack
     */
    public void push(T item) {
        this.next = new LinkedListItem<>(item, this.next);
    }

    /**
     * Pop from the stack
     * @return The first element of the stack
     */
    public T pop() {
        T item = this.next.getItem();
        this.next = this.next.getNext();
        return item;
    }
}
