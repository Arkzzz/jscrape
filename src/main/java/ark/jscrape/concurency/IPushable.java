package ark.jscrape.concurency;

public interface IPushable<T> {
    void push(T obj);
}
