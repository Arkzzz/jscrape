package ark.jscrape.util;

import java.util.Random;
import java.util.stream.Collectors;

public enum RandomString {
    UPPER_CASE(0x41, 0x5A),
    LOWER_CASE(0x61, 0x7A),
    ASCII(0x20, 0x7f),
    UNICODE(0xd800, 0x10ffff);
    private int from;
    private int to;
    RandomString(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public String get(int len) {
        return new Random().ints(from, to).limit(len).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining());
    }
}
